@testable import SkyApi
import XCTest

class CurrencyTests: XCTestCase {
    func test_DecodableFromJson() throws {
        let decoded = try JSONDecoder().decode(Currency.self, from: json)

        XCTAssertEqual(decoded, entity)
    }
}

private let json = try! [
    "Code": entity.id.rawValue,
    "Symbol": entity.symbol,
    "ThousandsSeparator": entity.thousandsSeparator,
    "DecimalSeparator": entity.decimalSeparator,
    "SymbolOnLeft": entity.symbolOnLeft,
    "SpaceBetweenAmountAndSymbol": entity.spaceBetweenAmountAndSymbol,
    "RoundingCoefficient": entity.roundingCoefficient,
    "DecimalDigits": entity.decimalDigits
].getJson()

private let entity = Currency(
    id: "RUB",
    symbol: "r",
    thousandsSeparator: ",",
    decimalSeparator: ".",
    symbolOnLeft: false,
    spaceBetweenAmountAndSymbol: true,
    roundingCoefficient: 1,
    decimalDigits: 2
)
