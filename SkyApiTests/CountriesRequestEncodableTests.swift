@testable import SkyApi
import XCTest

class CountriesRequestEncodableTests: XCTestCase {
    private let me = CountriesRequest(locale: "en-GB")

    func test_WhenAskedAboutHttpPath_ProvidesRelativeToBaseUrlWithLocale() throws {
        let httpPath = try me.httpPath()

        XCTAssertEqual(httpPath, .relativeToBase("\(CountriesRequest.relativePath)\(me.locale)"))
    }
}
