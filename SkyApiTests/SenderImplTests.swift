@testable import SkyApi
import SkyCore
import XCTest

class SenderImplTests: XCTestCase {
    private let dispatcher = ActionDispatcherFake()
    private let session = DataSessionFake()
    private lazy var me = SenderImpl(actionRunner: dispatcher.dispatch, dataSession: session.getTask)

    func test_WhenSendGetDecodable_PreparesRequest() {
        let encodable = GetEncodableFake()
        encodable.httpPathResult = .success(.relativeToBase(getPath))
        encodable.getParametersResult = .success(getParameters)

        let _ = me.send(to: baseUrl, of: DecodableFake.self, encodable: encodable)

        let request = session.calls.last
        XCTAssertEqual(request?.url, getUrl)
        XCTAssertEqual(request?.value(forHTTPHeaderField: "Accept"), "application/json")
    }

    func test_WhenSendGetDecodableAndItFails_UsesItError() {
        let encodable = GetEncodableFake()
        encodable.httpPathResult = .failure(ErrorFake.some)

        var result: DecodableResult<DecodableFake>?
        me.send(to: baseUrl, of: DecodableFake.self, encodable: encodable).result { result = $0 }
        dispatcher.finish()

        XCTAssertThrowsError(try result?.get())
    }

    func test_WhenSendPostDecodable_PreparesRequest() {
        let encodable = PostEncodableFake()
        encodable.httpPathResult = .success(.relativeToBase(postPath))
        encodable.postParametersResult = .success(postParameters)

        let _ = me.send(to: baseUrl, of: DecodableFake.self, encodable: encodable)

        let request = session.calls.last
        XCTAssertEqual(request?.url, postUrl)
        XCTAssertEqual(request?.httpBody, postBody)
        XCTAssertEqual(request?.value(forHTTPHeaderField: "Accept"), "application/json")
    }

    func test_WhenSendPostDecodableAndItFails_UsesItError() {
        let encodable = PostEncodableFake()
        encodable.httpPathResult = .failure(ErrorFake.some)

        var result: DecodableResult<DecodableFake>?
        me.send(to: baseUrl, of: DecodableFake.self, encodable: encodable).result { result = $0 }
        dispatcher.finish()

        XCTAssertThrowsError(try result?.get())
    }

    func test_WhenSendHttpDecodable_AndReceivesResponse_CreatesEncodableWithIt() throws {
        let encodable = GetEncodableFake()
        session.data = try JSONEncoder().encode(DecodableFake())
        session.response = HTTPURLResponse(url: baseUrl, statusCode: 200, httpVersion: "2.0", headerFields: [:])

        var result: DecodableResult<DecodableFake>?
        me.send(to: baseUrl, of: DecodableFake.self, encodable: encodable).result { result = $0 }
        dispatcher.finish()

        XCTAssertNotNil(result)
        XCTAssertNoThrow(try result?.get())
    }

    func test_WhenSendHttpDecodable_AndReceivesNot2xxResponse_CreatesFailureResult() throws {
        let encodable = GetEncodableFake()
        session.data = try JSONEncoder().encode(DecodableFake())
        session.response = HTTPURLResponse(url: baseUrl, statusCode: 300, httpVersion: "2.0", headerFields: [:])

        var result: DecodableResult<DecodableFake>?
        me.send(to: baseUrl, of: DecodableFake.self, encodable: encodable).result { result = $0 }
        dispatcher.finish()

        XCTAssertThrowsError(try result?.get())
    }

    func test_WhenSendHttpDecodable_AndReceivesNoData_CreatesFailureResult() throws {
        let encodable = GetEncodableFake()
        session.data = nil
        session.response = HTTPURLResponse(url: baseUrl, statusCode: 200, httpVersion: "2.0", headerFields: [:])

        var result: DecodableResult<DecodableFake>?
        me.send(to: baseUrl, of: DecodableFake.self, encodable: encodable).result { result = $0 }
        dispatcher.finish()

        XCTAssertThrowsError(try result?.get())
    }
}

private enum ErrorFake: Error {
    case some
}

private let baseUrl = URL(string: "http://scyscanner.com")!
private let getPath = "/path/"
private let getParameters: [String: Any] = ["key": "value"]
private let getUrl = URL(string: "http://scyscanner.com/path/?key=value")!

private let postPath = "/path/"
private let postParameters: [String: Any] = ["key": "value"]
private let postUrl = URL(string: "http://scyscanner.com/path/")!
private let postBody = "key=value".data(using: .utf8)!
