@testable import SkyApi
import XCTest

class CountryTests: XCTestCase {
    func test_DecodableFromJson() throws {
        let decoded = try JSONDecoder().decode(Country.self, from: json)

        XCTAssertEqual(decoded, entity)
    }
}

private let json = try! [
    "Code": entity.id.rawValue,
    "Name": entity.name
].getJson()

private let entity = Country(id: "GB", name: "Great Britain")
