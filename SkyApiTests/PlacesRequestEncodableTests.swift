@testable import SkyApi
import XCTest

class PlacesRequestEncodableTest: XCTestCase {
    private let me = PlacesRequest(
        country: "UK",
        currency: "GBP",
        locale: "en-GB",
        query: "Query"
    )

    func test_WhenAskedAboutHttpPath_ProvidesRelativeToBaseUrlWithCountryCurrencyLocale() throws {
        let httpPath = try me.httpPath()

        XCTAssertEqual(httpPath, .relativeToBase("\(PlacesRequest.relativePath)\(me.country)/\(me.currency)/\(me.locale)"))
    }

    func test_WhenAskedAboutParameters_ProvidesQuery() throws {
        let parameters = try me.getParameters()

        XCTAssertEqual(parameters[PlacesRequestCodingKeys.query.rawValue] as? String, me.query)
    }
}
