@testable import SkyApi
import XCTest

class LocaleTests: XCTestCase {
    func test_DecodableFromJson() throws {
        let decoded = try JSONDecoder().decode(SkyApi.Locale.self, from: json)

        XCTAssertEqual(decoded, entity)
    }
}

private let json = try! [
    "Code": entity.id.rawValue,
    "Name": entity.name
].getJson()

private let entity = Locale(id: "en-GB", name: "English")
