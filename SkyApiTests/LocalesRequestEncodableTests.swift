@testable import SkyApi
import XCTest

class LocalesRequestEncodableTest: XCTestCase {
    private let me = LocalesRequest()

    func test_WhenAskedAboutHttpPath_ProvidesRelativeToBaseUrl() throws {
        let httpPath = try me.httpPath()

        XCTAssertEqual(httpPath, .relativeToBase(LocalesRequest.relativePath))
    }
}
