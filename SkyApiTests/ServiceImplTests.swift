@testable import SkyApi
import XCTest

class ServiceImplTests: XCTestCase {
    private let sender = SenderFake()
    private lazy var me = ServiceImpl(baseUrl: baseUrl, apiKey: apiKey, sender: sender)

    func test_WhenSendsGetEncodable_AddsApiKeyToIt() throws {
        let encodable = GetEncodableFake()

        let _ = me.send(encodable, of: DecodableFake.self)

        let getCall = sender.getCalls.last
        let parameters = try getCall?.encodable.getParameters()
        XCTAssertEqual(parameters?[ClientKey.apiKey.rawValue] as? String, apiKey)
    }

    func test_WhenSendsGetEncodable_PassesHttpPathUntouchable() throws {
        let encodable = GetEncodableFake()
        let originPath = try encodable.httpPath()

        let _ = me.send(encodable, of: DecodableFake.self)

        let getCall = sender.getCalls.last
        let getParh = try getCall?.encodable.httpPath()
        XCTAssertEqual(originPath, getParh)
    }

    func test_WhenSendsPostEncodable_AddsApiKeyToIt() throws {
        let encodable = PostEncodableFake()

        let _ = me.send(encodable, of: DecodableFake.self)

        let getCall = sender.postCalls.last
        let parameters = try getCall?.encodable.postParameters()
        XCTAssertEqual(parameters?[ClientKey.apiKey.rawValue] as? String, apiKey)
    }

    func test_WhenSendsPostEncodable_PassesHttpPathUntouchable() throws {
        let encodable = PostEncodableFake()
        let originPath = try encodable.httpPath()

        let _ = me.send(encodable, of: DecodableFake.self)

        let postCall = sender.postCalls.last
        let postPath = try postCall?.encodable.httpPath()
        XCTAssertEqual(originPath, postPath)
    }

    func test_WhenSendsLocalesRequest_SendsItAsGetRequest() throws {
        let encodable = LocalesRequest()

        let _ = me.send(encodable)

        XCTAssertFalse(sender.getCalls.isEmpty)
    }

    func test_WhenSendsCurrenciesRequest_SendsItAsGetRequest() throws {
        let encodable = CurrenciesRequest()

        let _ = me.send(encodable)

        XCTAssertFalse(sender.getCalls.isEmpty)
    }

    func test_WhenSendsCountriesRequest_SendsItAsGetRequest() throws {
        let encodable = CountriesRequest(locale: "en-GB")

        let _ = me.send(encodable)

        XCTAssertFalse(sender.getCalls.isEmpty)
    }

    func test_WhenSendsPlacesRequest_SendsItAsGetRequest() throws {
        let encodable = PlacesRequest(
            country: "UK",
            currency: "GBP",
            locale: "en-GB",
            query: "Query"
        )

        let _ = me.send(encodable)

        XCTAssertFalse(sender.getCalls.isEmpty)
    }

    func test_WhenSendsSessionRequest_SendsItAsPostRequest() throws {
        let encodable = SessionRequest(
            country: "UK",
            locale: "en-GB",
            currency: "GBP",
            originPlace: "EDI",
            destinationPlace: "LHR",
            outboundDate: Date(),
            adults: 1
        )

        let _ = me.send(encodable)

        XCTAssertFalse(sender.postCalls.isEmpty)
    }

    func test_WhenSendsPricingRequest_SendsItAsGetRequest() throws {
        let encodable = PricingRequest(sessionUrl: baseUrl)

        let _ =  me.send(encodable)

        XCTAssertFalse(sender.getCalls.isEmpty)
    }
}

private let baseUrl = URL(string: "http://scyscanner.com")!
private let apiKey = "api"
