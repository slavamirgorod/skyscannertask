@testable import SkyApi
import XCTest

class LegTests: XCTestCase {
    func test_DecodableFromJson() throws {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .custom { _ in date }
        let decoded = try decoder.decode(Leg.self, from: json)

        XCTAssertEqual(decoded, entity)
    }
}

private let json = try! [
    "Id": entity.id.rawValue,
    "SegmentIds": entity.segmentIds.map { $0.rawValue },
    "OriginStation": entity.originStation.rawValue,
    "DestinationStation": entity.destinationStation.rawValue,
    "Departure": entity.departure.timeIntervalSince1970,
    "Arrival": entity.arrival.timeIntervalSince1970,
    "Duration": entity.duration,
    "Carriers": entity.carriers.map { $0.rawValue }
].getJson()

private let entity = Leg(
    id: "101",
    segmentIds: [102, 103],
    originStation: 50,
    destinationStation: 51,
    departure: date,
    arrival: date,
    duration: 10,
    carriers: [200]
)

private let date = Date()
