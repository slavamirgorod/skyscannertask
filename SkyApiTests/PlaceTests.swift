@testable import SkyApi
import XCTest

class PlaceTests: XCTestCase {
    func test_DecodableFromJson() throws {
        let decoded = try JSONDecoder().decode(Place.self, from: json)

        XCTAssertEqual(decoded, entity)
    }
}

private let json = try! [
    "PlaceId": entity.id.rawValue,
    "PlaceName": entity.placeName,
    "CountryId": entity.countryId.rawValue,
    "CityId": entity.cityId,
    "CountryName": entity.countryName
].getJson()

private let entity = Place(
    id: "MOW",
    placeName: "Moscow",
    countryId: "RU",
    cityId: "MOW",
    countryName: "Moscow"
)
