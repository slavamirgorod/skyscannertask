@testable import SkyApi
import XCTest

class PricingRequestEncodableTests: XCTestCase {
    private let me = PricingRequest(sessionUrl: sessionUrl)

    func test_WhenAskedAboutHttpPath_ProvidesAbsoluteSessionUrl() throws {
        let httpPath = try me.httpPath()

        XCTAssertEqual(httpPath, .absoluteUrl(sessionUrl))
    }
}

private let sessionUrl = URL(string: "http://scyscanner.com")!
