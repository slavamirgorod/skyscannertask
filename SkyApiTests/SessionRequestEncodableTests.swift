@testable import SkyApi
import XCTest

class SessionRequestEncodableTests: XCTestCase {
    private let me = SessionRequest(
        country: "UK",
        locale: "en-GB",
        currency: "GBP",
        originPlace: "MOW",
        destinationPlace: "TOW",
        outboundDate: Date(),
        inboundDate: Date(),
        cabinClass: .business,
        adults: 1,
        children: 2,
        infants: 3,
        includeCarriers: [101, 102],
        excludeCarriers: [103],
        groupPricing: true
    )

    func test_WhenAskedAboutHttpPath_ProvidesRelativeToBaseUrl() throws {
        let httpPath = try me.httpPath()

        XCTAssertEqual(httpPath, .relativeToBase(SessionRequest.relativePath))
    }

    func test_WhenAskedAboutParameters_ProvidesAllParameters() throws {
        let parameters = try me.postParameters()

        let params = [SessionRequestCodingKeys: Any](uniqueKeysWithValues: parameters.compactMap { key, value in
            SessionRequestCodingKeys(rawValue: key).map { ($0, value) }
        })
        XCTAssertEqual(params[.country] as? CountryID, me.country)
        XCTAssertEqual(params[.locale] as? LocaleID, me.locale)
        XCTAssertEqual(params[.currency] as? CurrencyID, me.currency)
        XCTAssertEqual(params[.originPlace] as? PlaceID, me.originPlace)
        XCTAssertEqual(params[.destinationPlace] as? PlaceID, me.destinationPlace)
        XCTAssertEqual(params[.cabinClass] as? CabinClass, me.cabinClass)
        XCTAssertEqual(params[.adults] as? Int, me.adults)
        XCTAssertEqual(params[.children] as? Int, me.children)
        XCTAssertEqual(params[.infants] as? Int, me.infants)
    }
}
