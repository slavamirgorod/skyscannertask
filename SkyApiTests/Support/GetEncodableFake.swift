//
//  GetEncodableFake.swift
//  SkyApiTests
//
//  
//
//

import Foundation
@testable import SkyApi

final class GetEncodableFake: GetEncodable {
    var httpPathResult = Result<HttpPath, Error>.success(.relativeToBase("path"))
    var getParametersResult = Result<[String: Any], Error>.success([:])

    func getParameters() throws -> [String: Any] {
        return try getParametersResult.get()
    }

    func httpPath() throws -> HttpPath {
        return try httpPathResult.get()
    }
}
