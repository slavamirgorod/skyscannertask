//
//  DataSessionFake.swift
//  SkyApiTests
//
//  
//
//

import Foundation
@testable import SkyApi

final class DataSessionFake {
    private final class Task: DataTask {
        let completion: DataCompletion
        let data: Data?
        let response: URLResponse?
        let error: Error?

        init(data: Data?,
             response: URLResponse?,
             error: Error?,
             completion: @escaping DataCompletion) {
            self.data = data
            self.response = response
            self.error = error
            self.completion = completion
        }

        func resume() {
            completion(data, response, error)
        }
    }

    var data: Data?
    var response: URLResponse?
    var error: Error?

    var calls: [URLRequest] = []

    func getTask(with request: URLRequest, completion: @escaping DataCompletion) -> DataTask {
        calls.append(request)
        return Task(data: data, response: response, error: error, completion: completion)
    }
}
