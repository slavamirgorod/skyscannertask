//
//  PostEncodableFake.swift
//  SkyApiTests
//
//  
//
//

import Foundation
@testable import SkyApi

final class PostEncodableFake: PostEncodable {
    var httpPathResult = Result<HttpPath, Error>.success(.relativeToBase("path"))
    var postParametersResult = Result<[String: Any], Error>.success([:])

    func postParameters() throws -> [String: Any] {
        return try postParametersResult.get()
    }

    func httpPath() throws -> HttpPath {
        return try httpPathResult.get()
    }
}
