//
//  JsonDictionary.swift
//  SkyApiTests
//
//  
//
//

import Foundation

extension Dictionary where Key == String {
    func getJson() throws -> Data {
        return try JSONSerialization.data(withJSONObject: self, options: [])
    }
}
