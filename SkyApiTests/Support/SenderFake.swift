import Foundation
@testable import SkyApi
import SkyCore

final class SenderFake: Sender {
    struct GetCall {
        let url: URL
        let encodable: GetEncodable
        let feed: Any
    }

    struct PostCall {
        let url: URL
        let encodable: PostEncodable
        let feed: Any
    }

    private(set) var getCalls = [GetCall]()
    private(set) var postCalls = [PostCall]()

    func send<T>(to url: URL, of _: T.Type, encodable: GetEncodable) -> Future<T> where T : Decodable {
        let (future, feed) = Future<T>.create()
        getCalls.append(.init(url: url, encodable: encodable, feed: feed))
        return future
    }

    func send<T>(to url: URL, of _: T.Type, encodable: PostEncodable) -> Future<T> where T : Decodable {
        let (future, feed) = Future<T>.create()
        postCalls.append(.init(url: url, encodable: encodable, feed: feed))
        return future
    }
}
