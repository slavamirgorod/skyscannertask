@testable import SkyApi
import XCTest

class CurrenciesRequestEncodableTest: XCTestCase {
    private let me = CurrenciesRequest()

    func test_WhenAskedAboutHttpPath_ProvidesRelativeToBaseUrl() throws {
        let httpPath = try me.httpPath()

        XCTAssertEqual(httpPath, .relativeToBase(CurrenciesRequestKeys.path))
    }
}
