import Foundation
import SkyCore

public typealias ServiceCompletion<U: Decodable> = (Result<U, Error>) -> Void

public protocol Service {
    func send<T: Decodable>(_ encodable: GetEncodable, of type: T.Type) -> Future<T>
    func send<T: Decodable>(_ encodable: PostEncodable, of type: T.Type) -> Future<T>
}

public extension Service {
    func send(_ request: LocalesRequest) -> Future<LocalesResponse> {
        return send(request, of: LocalesResponse.self)
    }

    func send(_ request: CurrenciesRequest)  -> Future<CurrenciesResponse> {
        return send(request, of: CurrenciesResponse.self)
    }

    func send(_ request: CountriesRequest) -> Future<CountriesResponse> {
        return send(request, of: CountriesResponse.self)
    }

    func send(_ request: PlacesRequest) -> Future<PlacesResponse> {
        return send(request, of: PlacesResponse.self)
    }

    func send(_ request: SessionRequest) -> Future<SessionResponse> {
        return send(request, of: SessionResponse.self)
    }

    func send(_ request: PricingRequest) -> Future<PricingResponse> {
       return send(request, of: PricingResponse.self)
    }
}

public func makeService() -> Service {
    return ServiceImpl(
        baseUrl: URL(string: "http://partners.api.skyscanner.net/apiservices/")!,
        apiKey: "ss630745725358065467897349852985",
        sender: SenderImpl(
            actionRunner: dispatchOnMainQueue,
            dataSession: URLSession.shared.getDataTask
        )
    )
}
