//
//  PricingOption.swift
//  SkyApi
//
//  .
//
//

import Foundation

public struct PricingOption: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case quoteAgeInMinutes = "QuoteAgeInMinutes"
        case price = "Price"
        case agents = "Agents"
        case deeplinkUrl = "DeeplinkUrl"
    }

    public let quoteAgeInMinutes: Double
    public let price: Double
    public let agents: [AgentID]
    public let deeplinkUrl: URL?
}
