import Foundation
import SkyCore

public enum CurrencyTag {}
public enum PlaceTag {}
public enum LocaleTag {}
public enum CountryTag {}
public enum LegTag {}
public enum AgentTag {}
public enum SegmentTag {}
public enum StationTag {}
public enum CarrierTag {}

public typealias CurrencyID = Tagged<CurrencyTag, String>
public typealias PlaceID = Tagged<PlaceTag, String>
public typealias LocaleID = Tagged<LocaleTag, String>
public typealias CountryID = Tagged<CountryTag, String>
public typealias LegID = Tagged<LegTag, String>
public typealias SegmentID = Tagged<SegmentTag, Int>
public typealias StationID = Tagged<StationTag, Int>
public typealias AgentID = Tagged<AgentTag, Int>
public typealias CarrierID = Tagged<CarrierTag, Int>
