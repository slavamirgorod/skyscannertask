import Foundation
import SkyCore

public struct Currency: Decodable, Identifiable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case id = "Code"
        case symbol = "Symbol"
        case thousandsSeparator = "ThousandsSeparator"
        case decimalSeparator = "DecimalSeparator"
        case symbolOnLeft = "SymbolOnLeft"
        case spaceBetweenAmountAndSymbol = "SpaceBetweenAmountAndSymbol"
        case roundingCoefficient = "RoundingCoefficient"
        case decimalDigits = "DecimalDigits"
    }

    public let id: CurrencyID
    public let symbol: String
    public let thousandsSeparator: String
    public let decimalSeparator: String
    public let symbolOnLeft: Bool
    public let spaceBetweenAmountAndSymbol: Bool
    public let roundingCoefficient: Int
    public let decimalDigits: Int
}
