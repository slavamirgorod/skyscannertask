import Foundation
import SkyCore

public struct Carrier: Decodable, Identifiable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case code = "Code"
        case name = "Name"
        case imageUrl = "ImageUrl"
        case displayCode = "DisplayCode"
    }

    public let id: CarrierID
    public let code: String
    public let name: String
    public let imageUrl: URL
    public let displayCode: String
}
