import Foundation
import SkyCore

public struct Locale: Decodable, Equatable, Identifiable {
    private enum CodingKeys: String, CodingKey {
        case id = "Code"
        case name = "Name"
    }

    public let id: LocaleID
    public let name: String
}
