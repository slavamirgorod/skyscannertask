import Foundation
import SkyCore

public struct Leg: Decodable, Equatable, Identifiable {
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case segmentIds = "SegmentIds"
        case originStation = "OriginStation"
        case destinationStation = "DestinationStation"
        case departure = "Departure"
        case arrival = "Arrival"
        case duration = "Duration"
        case carriers = "Carriers"
    }

    public let id: LegID
    public let segmentIds: [SegmentID]
    public let originStation: StationID
    public let destinationStation: StationID
    public let departure: Date
    public let arrival: Date
    public let duration: Int
    public let carriers: [CarrierID]
}
