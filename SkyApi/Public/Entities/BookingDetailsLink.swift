import Foundation

public struct BookingDetailsLink: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case uri = "Uri"
        case body = "Body"
        case method = "Method"
    }

    public let uri: String
    public let body: String
    public let method: String
}
