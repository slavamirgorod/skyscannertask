//
//  Station.swift
//  SkyApi
//
//  .
//
//

import Foundation
import SkyCore

public struct Station: Decodable, Identifiable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case parentId = "ParentId"
        case code = "Code"
        case type = "Type"
        case name = "Name"
    }

    public let id: StationID
    public let parentId: StationID?
    public let code: String
    public let type: String
    public let name: String
}
