//
//  PricingStatus.swift
//  SkyApi
//
//  .
//
//

import Foundation

public enum UpdatesStatus: String, Decodable, Equatable {
    case pending = "UpdatesPending"
    case complete = "UpdatesComplete"
}
