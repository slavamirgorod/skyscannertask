import Foundation

public enum CabinClass: String, Decodable {
    case economy
    case premiumEconomy = "premiumeconomy"
    case business
    case first
}
