//
//  Agent.swift
//  SkyApi
//
//  Created by Viacheslav Mirgorod on 5/12/19.
//  Copyright © 2019 Anonimous. All rights reserved.
//

import Foundation
import SkyCore

public struct Agent: Decodable, Identifiable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case imageUrl = "ImageUrl"
        case status = "Status"
        case optimisedForMobile = "OptimisedForMobile"
        case bookingNumber = "BookingNumber"
        case type = "Type"
    }

    public let id: AgentID
    public let name: String
    public let imageUrl: URL
    public let status: UpdatesStatus
    public let optimisedForMobile: Bool
    public let bookingNumber: String?
    public let type: String
}
