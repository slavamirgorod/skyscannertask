import Foundation
import SkyCore

public struct Place: Decodable, Equatable, Identifiable {
    private enum CodingKeys: String, CodingKey {
        case id = "PlaceId"
        case placeName = "PlaceName"
        case countryId = "CountryId"
        case cityId = "CityId"
        case countryName = "CountryName"
    }

    public let id: PlaceID
    public let placeName: String
    public let countryId: CountryID
    public let cityId: String
    public let countryName: String
}
