import Foundation
import SkyCore

public struct Country: Decodable, Equatable, Identifiable {
    private enum CodingKeys: String, CodingKey {
        case id = "Code"
        case name = "Name"
    }

    public let id: CountryID
    public let name: String
}
