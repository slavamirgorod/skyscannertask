import Foundation

public struct Itinerary: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case outboundLegId = "OutboundLegId"
        case inboundLegId = "InboundLegId"
        case pricingOptions = "PricingOptions"
        case bookingDetailsLink = "BookingDetailsLink"
    }

    public let outboundLegId: LegID
    public let inboundLegId: LegID?
    public let pricingOptions: [PricingOption]
    public let bookingDetailsLink: BookingDetailsLink
}

public extension Itinerary {
    var cheapestPrice: Double {
        return pricingOptions.first?.price ?? 0
    }

    var cheapestPricingOption: PricingOption {
        // we are assuming here there can't be itinerary without pricing option
        return pricingOptions.min(by: { $0.price < $1.price })!
    }

    func outboundLegDuration(using legs: [LegID: Leg]) -> Int {
        if let leg = legs[outboundLegId] {
            return leg.duration
        }
        return 0
    }

    func inboundLegDuration(using legs: [LegID: Leg]) -> Int {
        if let inboundLegId = inboundLegId, let leg = legs[inboundLegId] {
            return leg.duration
        }
        return 0
    }

    func legsDuration(using legs: [LegID: Leg]) -> Int {
        return outboundLegDuration(using: legs) + inboundLegDuration(using: legs)
    }

    func legs(using legs: [LegID: Leg]) -> [Leg] {
        return [inboundLegId, outboundLegId].compactMap { $0.map { legs[$0] } ?? nil }
    }
}

public extension Array where Element == Itinerary {
    func cheapestPrices(limit: Int) -> [Double] {
        let values = Set(map { $0.cheapestPrice }).sorted { $0 < $1 }
        return [Double](values[..<Swift.min(values.count, limit)])
    }

    func lowestDurations(using legs: [LegID: Leg], limit: Int) -> [Int] {
        let values = Set(map { $0.legsDuration(using: legs) }).sorted { $0 < $1 }
        return [Int](values[..<Swift.min(values.count, limit)])
    }
}
