//
//  PricinigQuery.swift
//  SkyApi
//
//  .
//
//

import Foundation

public struct PricingQuery: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case country = "Country"
        case locale = "Locale"
        case currency = "Currency"
        case adults = "Adults"
        case children = "Children"
        case infants = "Infants"
        case originPlace = "OriginPlace"
        case destinationPlace = "DestinationPlace"
        case outboundDate = "OutboundDate"
        case inboundDate = "InboundDate"
        case cabinClass = "CabinClass"
        case groupPricing = "GroupPricing"
    }

    public let country: CountryID
    public let locale: LocaleID
    public let currency: CurrencyID
    public let adults: Int
    public let children: Int?
    public let infants: Int?
    public let originPlace: PlaceID
    public let destinationPlace: PlaceID
    public let outboundDate: String
    public let inboundDate: String?
    public let cabinClass: CabinClass?
    public let groupPricing: Bool?

    public static let empty = PricingQuery(
        country: "EN",
        locale: "en-GB",
        currency: "GBR",
        adults: 1,
        children: nil,
        infants: nil,
        originPlace: "MOW",
        destinationPlace: "TOW",
        outboundDate: "",
        inboundDate: nil,
        cabinClass: nil,
        groupPricing: nil
    )
}
