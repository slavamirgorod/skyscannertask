//
//  MarketsResponse.swift
//  SkyApi
//
//
//
//

import Foundation

public struct CountriesResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case countries = "Countries"
    }

    public let countries: [Country]
}
