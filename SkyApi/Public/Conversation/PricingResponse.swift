import Foundation

public struct PricingResponse: Decodable, Equatable {
    private enum CodingKeys: String, CodingKey {
        case query = "Query"
        case legs = "Legs"
        case itineraries = "Itineraries"
        case carriers = "Carriers"
        case places = "Places"
        case status = "Status"
        case currencies = "Currencies"
        case agents = "Agents"
    }

    public let query: PricingQuery
    public let status: UpdatesStatus
    public let legs: [Leg]
    public let carriers: [Carrier]
    public let places: [Station]
    public let itineraries: [Itinerary]
    public let currencies: [Currency]
    public let agents: [Agent]

    public init(
        query: PricingQuery,
        status: UpdatesStatus,
        legs: [Leg],
        carriers: [Carrier],
        places: [Station],
        itineraries: [Itinerary],
        currencies: [Currency],
        agents: [Agent]) {
        self.query = query
        self.status = status
        self.legs = legs
        self.carriers = carriers
        self.places = places
        self.itineraries = itineraries
        self.currencies = currencies
        self.agents = agents
    }
}
