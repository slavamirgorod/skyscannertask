//
//  CurrenciesResponse.swift
//  SkyApi
//
//
//
//

import Foundation

public struct CurrenciesResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case currencies = "Currencies"
    }

    public let currencies: [Currency]
}
