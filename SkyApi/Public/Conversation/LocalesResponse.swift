//
//  LocalesResponse.swift
//  SkyApi
//
//
//
//

import Foundation

public struct LocalesResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case locales = "Locales"
    }

    public let locales: [Locale]
}
