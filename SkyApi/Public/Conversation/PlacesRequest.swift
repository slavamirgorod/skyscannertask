//
//  PlacesRequest.swift
//  SkyApi
//
//
//
//

import Foundation

public struct PlacesRequest {
    /// The market/country your user is in
    public let country: CountryID

    /// The currency you want the prices in
    public let currency: CurrencyID

    /// The locale you want the results in (ISO locale)
    public let locale: LocaleID

    /// The query string, must be at least 2 characters long.
    public let query: String

    public init(country: CountryID, currency: CurrencyID, locale: LocaleID, query: String) {
        self.country = country
        self.currency = currency
        self.locale = locale
        self.query = query
    }
}
