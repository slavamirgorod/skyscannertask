//
//  MarketRequest.swift
//  SkyApi
//
//
//
//

import Foundation

public struct CountriesRequest {
    /// The locale you want the results in (ISO locale)
    public let locale: LocaleID

    public init(locale: LocaleID) {
        self.locale = locale
    }
}
