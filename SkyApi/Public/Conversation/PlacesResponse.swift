//
//  PlacesResponse.swift
//  SkyApi
//
//
//
//

import Foundation

public struct PlacesResponse: Decodable {
    private enum CodingKeys: String, CodingKey {
        case places = "Places"
    }

    public let places: [Place]
}
