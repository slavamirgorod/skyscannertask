//
//  SkySession.swift
//  SkyApi
//
//
//
//

import Foundation

public struct SessionRequest {
    /// The market/country your user is in
    public let country: CountryID

    /// The locale you want the results in (ISO locale)
    public let locale: LocaleID

    /// The currency you want the prices in
    public let currency: CurrencyID

    /// The origin place (see places)
    public let originPlace: PlaceID

    /// The destination place (see places)
    public let destinationPlace: PlaceID

    /// The outbound date. Format “yyyy-mm-dd”.
    public let outboundDate: Date

    /// The return date. Format “yyyy-mm-dd”. Use empty string for oneway trip.
    public let inboundDate: Date?

    /// The cabin class. Can be “economy”, “premiumeconomy”, “business”, “first”
    public let cabinClass: CabinClass?

    /// Number of adults (16+ years). Must be between 1 and 8.
    public let adults: Int

    /// Number of children (1-16 years). Can be between 0 and 8.
    public let children: Int?

    /// Number of infants (under 12 months). Can be between 0 and 8.
    public let infants: Int?

    /// Only return results from those carriers. Comma-separated list of carrier ids.
    public let includeCarriers: [CarrierID]?

    /// Filter out results from those carriers. Comma-separated list of carrier ids.
    public let excludeCarriers: [CarrierID]?

    /// If set to true, prices will be obtained for the whole passenger group and
    /// if set to false it will be obtained for one adult. By default it is set to false.
    public let groupPricing: Bool?

    public init(
        country: CountryID,
        locale: LocaleID,
        currency: CurrencyID,
        originPlace: PlaceID,
        destinationPlace: PlaceID,
        outboundDate: Date,
        inboundDate: Date? = nil,
        cabinClass: CabinClass? = nil,
        adults: Int,
        children: Int? = nil,
        infants: Int? = nil,
        includeCarriers: [CarrierID]? = nil,
        excludeCarriers: [CarrierID]? = nil,
        groupPricing: Bool? = nil
    ) {
        self.country = country
        self.locale = locale
        self.currency = currency
        self.originPlace = originPlace
        self.destinationPlace = destinationPlace
        self.outboundDate = outboundDate
        self.inboundDate = inboundDate
        self.cabinClass = cabinClass
        self.adults = adults
        self.children = children
        self.infants = infants
        self.includeCarriers = includeCarriers
        self.excludeCarriers = excludeCarriers
        self.groupPricing = groupPricing
    }
}
