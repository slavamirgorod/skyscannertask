//
//  None.swift
//  SkyApi
//
//
//
//

import Foundation

public struct SessionResponse: Decodable {
    enum Error: Swift.Error {
        case noLocation
        case invalidUrl(String)
    }

    public let location: URL

    public init(location: URL) {
        self.location = location
    }

    public init(from decoder: Decoder) throws {
        guard let location = decoder.allHeaderFields["Location"] else {
            throw Error.noLocation
        }

        guard let locationUrl = URL(string: location) else {
            throw Error.invalidUrl(location)
        }

        self.location = locationUrl
    }
}
