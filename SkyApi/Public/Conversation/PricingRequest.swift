//
//  Pricing.swift
//  SkyApi
//
//
//
//

import Foundation

public struct PricingRequest {
    public let sessionUrl: URL

    public init(sessionUrl: URL) {
        self.sessionUrl = sessionUrl
    }
}
