//
//  PostEncodable.swift
//  SkyApi
//
//
//
//

import Foundation

public protocol PostEncodable: HttpEncodable {
    func postParameters() throws -> [String: Any]
}
