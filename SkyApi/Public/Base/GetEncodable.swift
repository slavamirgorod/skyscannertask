//
//  GetEncodable.swift
//  SkyApi
//
//
//
//

import Foundation

public protocol GetEncodable: HttpEncodable {
    func getParameters() throws -> [String: Any]
}
