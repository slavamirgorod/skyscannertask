//
//  HttpEncodable.swift
//  SkyApi
//
//  .
//
//

import Foundation

public enum HttpPath: Equatable {
    case relativeToBase(String)
    case absoluteUrl(URL)
}

public protocol HttpEncodable {
    func httpPath() throws -> HttpPath
}
