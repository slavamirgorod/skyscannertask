//
//  DataRunner.swift
//  SkyApi
//
//
//
//

import Foundation

typealias DataCompletion = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void

typealias DataSession = (_ urlRequest: URLRequest, _ completion: @escaping DataCompletion) -> DataTask

extension URLSession {
    func getDataTask(with urlRequest: URLRequest, completion: @escaping DataCompletion) -> DataTask {
        return dataTask(with: urlRequest, completionHandler: completion)
    }
}
