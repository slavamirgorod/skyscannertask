import Foundation

protocol DataTask {
    func resume()
}
