import Foundation
import SkyCore

typealias SenderCompletion<T: Decodable> = (DecodableResult<T>) -> Void

protocol Sender {
    func send<T: Decodable>(
        to url: URL,
        of _: T.Type,
        encodable: GetEncodable) -> Future<T>

    func send<T: Decodable>(
        to url: URL,
        of _: T.Type,
        encodable: PostEncodable) -> Future<T>
}
