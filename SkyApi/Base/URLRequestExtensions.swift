//
//  URLRequestExtensions.swift
//  SkyApi
//
//
//
//

import Foundation

extension URLRequest {
    mutating func setHTTPBodyParameters(_ parameters: [String: Any]) {
        let data = parameters.percentEscaped().data(using: .utf8)!
        httpMethod = "POST"
        httpBody = data
        setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        setValue("\(data.count)", forHTTPHeaderField: "Content-Length")
    }
}

private extension Dictionary {
    func percentEscaped() -> String {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }.joined(separator: "&")
    }
}

private extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
