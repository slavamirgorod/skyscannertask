//
//  DataDecodable.swift
//  SkyApi
//
//
//
//

import Foundation

typealias DecodableResult<T: Decodable> = Result<T, Error>

extension CodingUserInfoKey {
    static let allHeaderFields = CodingUserInfoKey(rawValue: "CodingUserInfoKey.allHeaderFields")!
}

extension Decoder {
    var allHeaderFields: [AnyHashable: String] {
        let value = userInfo[.allHeaderFields] as? [AnyHashable: String]
        return value ?? [:]
    }
}
