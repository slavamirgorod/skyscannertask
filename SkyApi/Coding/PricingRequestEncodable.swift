//
//  PricingRequestEncodable.swift
//  SkyApi
//
//  .
//
//

import Foundation

extension PricingRequest: GetEncodable {
    public func httpPath() throws -> HttpPath {
        return .absoluteUrl(sessionUrl)
    }

    public func getParameters() throws -> [String: Any] {
        return [:]
    }
}
