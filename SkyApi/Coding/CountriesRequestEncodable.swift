//
//  MarketRequestEncodable.swift
//  SkyApi
//
//
//
//

import Foundation

extension CountriesRequest: GetEncodable {
    static let relativePath = "/reference/v1.0/countries/"

    public func httpPath() throws -> HttpPath {
        return .relativeToBase("\(CountriesRequest.relativePath)\(locale)")
    }

    public func getParameters() throws -> [String: Any] {
        return [:]
    }
}
