//
//  PlacesRequestEncodable.swift
//  SkyApi
//
//
//
//

import Foundation

extension PlacesRequest: GetEncodable {
    static let relativePath = "/autosuggest/v1.0/"

    public func httpPath() throws -> HttpPath {
        return .relativeToBase("\(PlacesRequest.relativePath)\(country)/\(currency)/\(locale)")
    }

    public func getParameters() throws -> [String: Any] {
        let value: [PlacesRequestCodingKeys: Any] = [
            .query: query,
        ].filteringNilValues()
        return Dictionary(uniqueKeysWithValues: value.map { key, value in (key.rawValue, value) })
    }
}

enum PlacesRequestCodingKeys: String {
    case query
}
