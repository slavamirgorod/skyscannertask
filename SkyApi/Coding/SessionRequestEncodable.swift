//
//  SessionEncodable.swift
//  SkyApi
//
//
//
//

import Foundation

extension SessionRequest: PostEncodable {
    static let relativePath = "pricing/v1.0/"

    public func httpPath() throws -> HttpPath {
        return .relativeToBase(SessionRequest.relativePath)
    }

    public func postParameters() throws -> [String: Any] {
        let value: [SessionRequestCodingKeys: Any] = [
            .country: country,
            .locale: locale,
            .currency: currency,
            .originPlace: originPlace,
            .destinationPlace: destinationPlace,
            .outboundDate: outboundDate.yearMonthDayString,
            .inboundDate: inboundDate?.yearMonthDayString,
            .cabinClass: cabinClass,
            .adults: adults,
            .children: children,
            .infants: infants,
            .includeCarriers: includeCarriers.map { $0.map { "\($0)" }.joined(separator: ",") },
            .excludeCarriers: excludeCarriers.map { $0.map { "\($0)" }.joined(separator: ",") },
            .groupPricing: groupPricing,
            .locationSchema: "iata",
        ].filteringNilValues()
        return Dictionary(uniqueKeysWithValues: value.map { key, value in (key.rawValue, value) })
    }
}

enum SessionRequestCodingKeys: String {
    case country, locale, currency
    case originPlace, destinationPlace, outboundDate
    case inboundDate, cabinClass, adults
    case children, infants, includeCarriers
    case excludeCarriers, groupPricing, locationSchema
}
