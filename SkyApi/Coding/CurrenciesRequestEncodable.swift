//
//  CurrenciesRequestEncodable.swift
//  SkyApi
//
//
//
//

import Foundation

extension CurrenciesRequest: GetEncodable {
    public func httpPath() throws -> HttpPath {
        return .relativeToBase(CurrenciesRequestKeys.path)
    }

    public func getParameters() throws -> [String: Any] {
        return [:]
    }
}

enum CurrenciesRequestKeys {
    static let path = "/reference/v1.0/currencies/"
}
