//
//  LocalesRequestEncodable.swift
//  SkyApi
//
//
//
//

import Foundation

extension LocalesRequest: GetEncodable {
    static let relativePath = "/reference/v1.0/locales/"

    public func httpPath() throws -> HttpPath {
        return .relativeToBase(LocalesRequest.relativePath)
    }

    public func getParameters() throws -> [String: Any] {
        return [:]
    }
}
