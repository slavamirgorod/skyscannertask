import Foundation
import SkyCore

final class ServiceImpl: Service {
    func send<T: Decodable>(_ encodable: GetEncodable, of type: T.Type) -> Future<T> {
        let augmentedEncodable = ApiGetEncodable(apiKey: apiKey, subject: encodable)
        return sender.send(to: baseUrl, of: type, encodable: augmentedEncodable)
    }

    func send<T: Decodable>(_ encodable: PostEncodable, of type: T.Type) ->  Future<T> {
        let augmentedEncodable = ApiPostEncodable(apiKey: apiKey, subject: encodable)
        return sender.send(to: baseUrl, of: type, encodable: augmentedEncodable)
    }

    init(baseUrl: URL, apiKey: String, sender: Sender) {
        self.baseUrl = baseUrl
        self.apiKey = apiKey
        self.sender = sender
    }

    private struct ApiGetEncodable: GetEncodable {
        func httpPath() throws -> HttpPath {
            return try subject.httpPath()
        }

        func getParameters() throws -> [String: Any] {
            var dict = try subject.getParameters()
            dict[ClientKey.apiKey.rawValue] = apiKey
            return dict
        }

        let apiKey: String
        let subject: GetEncodable
    }

    private struct ApiPostEncodable: PostEncodable {
        func httpPath() throws -> HttpPath {
            return try subject.httpPath()
        }

        func postParameters() throws -> [String: Any] {
            var dict = try subject.postParameters()
            dict[ClientKey.apiKey.rawValue] = apiKey
            return dict
        }

        let apiKey: String
        let subject: PostEncodable
    }

    private let baseUrl: URL
    private let apiKey: String
    private let sender: Sender
}

enum ClientKey: String {
    case apiKey
}
