import Foundation
import SkyCore

final class SenderImpl: Sender {
    enum Error: Swift.Error {
        case unrecognizedResponse
        case noData
        case inexpectedHttpStatus(Int)
        case inexpectedUrl(URL)
        case inexpectedUrlComponents(URLComponents)
    }

    func send<T: Decodable>(
        to url: URL,
        of type: T.Type,
        encodable: GetEncodable) -> Future<T> {
        let (future, feed) = Future.create(of: T.self)
        do {
            let newUrl = try encodable.prepareGetUrl(url)
            var request = URLRequest(url: newUrl)
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            sendRequest(request, of: type, completion: feed)
        } catch {
            actionRunner { feed(.failure(error)) }
        }
        return future
    }

    func send<T: Decodable>(
        to url: URL,
        of type: T.Type,
        encodable: PostEncodable) -> Future<T> {
        let (future, feed) = Future.create(of: T.self)
        do {
            let parameters = try encodable.postParameters()
            let newUrl = try encodable.prepareUrl(url)
            var request = URLRequest(url: newUrl)
            request.setHTTPBodyParameters(parameters)
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            sendRequest(request, of: type, completion: feed)
        } catch {
            actionRunner { feed(.failure(error)) }
        }
        return future
    }

    init(actionRunner: @escaping ActionDispatcher, dataSession: @escaping DataSession) {
        self.actionRunner = actionRunner
        self.dataSession = dataSession
    }

    private typealias Result = (data: Data?, response: URLResponse?, error: Swift.Error?)

    private let actionRunner: ActionDispatcher
    private let dataSession: DataSession

    private func sendRequest<T: Decodable>(
        _ request: URLRequest,
        of type: T.Type,
        completion: @escaping SenderCompletion<T>
    ) {
        func process(_ data: Data?, _ response: URLResponse?, _ error: Swift.Error?) throws -> T {
            if let error = error {
                throw error
            }

            guard let response = response as? HTTPURLResponse else {
                throw Error.unrecognizedResponse
            }

            guard (200 ... 299) ~= response.statusCode else {
                throw Error.inexpectedHttpStatus(response.statusCode)
            }

            guard let data = data else {
                throw Error.noData
            }

            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted({
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                return dateFormatter
            }())
            decoder.userInfo[.allHeaderFields] = response.allHeaderFields
            return try decoder.decode(type, from: data)
        }

        let task = dataSession(request) { [actionRunner] data, response, error in
            do {
                let item = try process(data, response, error)
                actionRunner { completion(.success(item)) }
            } catch {
                actionRunner { completion(.failure(error)) }
            }
        }
        task.resume()
    }
}

private extension HttpEncodable {
    func prepareUrl(_ url: URL) throws -> URL {
        let path = try httpPath()
        switch path {
        case let .absoluteUrl(newUrl): return newUrl
        case let .relativeToBase(path): return url.appendingPathComponent(path)
        }
    }
}

private extension GetEncodable {
    func prepareGetUrl(_ url: URL) throws -> URL {
        let parameters = try getParameters()
        let newUrl = try prepareUrl(url)
        guard var urlComps = URLComponents(url: newUrl, resolvingAgainstBaseURL: false) else {
            throw SenderImpl.Error.inexpectedUrl(url)
        }
        urlComps.queryItems = parameters.map { URLQueryItem(name: $0.key, value: "\($0.value)") }
        guard let finalUrl = urlComps.url else {
            throw SenderImpl.Error.inexpectedUrlComponents(urlComps)
        }
        return finalUrl
    }
}
