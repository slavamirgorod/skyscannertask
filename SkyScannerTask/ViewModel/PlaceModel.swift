import Foundation

extension UI {
    struct PlaceModel {
        let id: String
        let name: String
    }
}
