import Foundation

extension UI {
    enum PricingState {
        case requested
        case progress(Float)
        case failed
        case completed
    }

    struct PricingModel {
        let state: PricingState
        let results: String
        let stations: NSAttributedString
        let time: NSAttributedString
        let iteneraries: [ItineraryModel]
    }
}
