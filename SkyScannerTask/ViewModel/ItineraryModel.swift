import Foundation

extension UI {
    struct ItineraryModel {
        let price: String
        let notes: NSAttributedString
        let legs: [LegModel]
    }
}
