import Foundation

extension UI {
    struct LegModel {
        let time: NSAttributedString
        let duration: String
        let segments: String
        let directness: String
        let carrierImage: ImageSource
    }
}
