import UIKit

extension UIViewController {
    @objc
    var prefersNavigationBarHidden: Bool {
        return false
    }

    func updateNavigationBarState(animated: Bool) {
        navigationController?.setNavigationBarHidden(prefersNavigationBarHidden, animated: animated)
    }
}

