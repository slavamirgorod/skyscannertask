import CoreGraphics

extension CGRect {
    init(x: CGFloat, centerY: CGFloat, width: CGFloat, height: CGFloat) {
        self.init(x: x, y: centerY - height / 2, width: width, height: height)
    }

    init(x: CGFloat, bottomY: CGFloat, width: CGFloat, height: CGFloat) {
        self.init(x: x, y: bottomY - height, width: width, height: height)
    }

    init(x: CGFloat, centerY: CGFloat, side: CGFloat) {
        self.init(x: x, y: centerY - side / 2, width: side, height: side)
    }

    init(rightX: CGFloat, bottomY: CGFloat, width: CGFloat, height: CGFloat) {
        self.init(x: rightX - width, y: bottomY - height, width: width, height: height)
    }

    init(rightX: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) {
        self.init(x: rightX - width, y: y, width: width, height: height)
    }
}
