import Foundation

extension NSMutableAttributedString {
    func append(_ string: String, attributes: [NSAttributedString.Key: Any]?) {
        append(NSAttributedString(
            string: string,
            attributes: attributes
        ))
    }
}
