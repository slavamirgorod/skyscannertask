import UIKit

extension UI {
    enum BrandColor {
        static let gray = UIColor(red: 0.655, green: 0.635, blue: 0.686, alpha: 1.0)
        static let green = UIColor(red: 0, green: 0.843, blue: 0.459, alpha: 1.0)
        static let red = UIColor.red
    }
}
