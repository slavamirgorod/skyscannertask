import UIKit

enum ImageSource {
    case image(UIImage)
    case url(URL)
}

extension UrlImageView {
    func setImageSource(_ imageSource: ImageSource) {
        switch imageSource {
        case let .image(image): self.image = image
        case let .url(url): imageUrl = url
        }
    }
}
