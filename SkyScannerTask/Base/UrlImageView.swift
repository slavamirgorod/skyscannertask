import UIKit

final class UrlImageView: UIImageView {
    override var image: UIImage? {
        didSet {
            _imageUrl = nil
        }
    }

    var imageUrl: URL? {
        get {
            return _imageUrl
        }
        set {
            guard _imageUrl != newValue else {
                return
            }

            _imageUrl = newValue

            if let imageUrl = _imageUrl {
                loadImage(from: imageUrl)
            } else {
                setImage(nil)
            }
        }
    }

    private var _imageUrl: URL?

    private func setImage(_ image: UIImage?) {
        super.image = image
    }

    private func loadImage(from url: URL, cache: URLCache = .shared) {
        let request = URLRequest(url: url)
        if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
            setImage(image)
            _imageUrl = url
        } else {
            _imageUrl = url
            URLSession.shared.dataTask(with: request, completionHandler: { [weak self] data, response, _ in
                if let self = self,
                    let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode < 300,
                    self._imageUrl == url,
                    let image = UIImage(data: data) {
                    let cachedData = CachedURLResponse(response: response, data: data)
                    cache.storeCachedResponse(cachedData, for: request)
                    DispatchQueue.main.async {
                        self.setImage(image)
                    }
                }
            }).resume()
        }
    }
}
