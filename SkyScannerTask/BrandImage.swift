import UIKit

extension UI {
    enum BrandImage {
        static let multiCarrier = UIImage(named: "multi-carrier")!
        static let flight = UIImage(named: "flight")!
    }
}
