import Foundation
import SkyApi
import SkyCore

final class ItineraryListInteractorImpl: ItineraryListInteractor {
    var country = "UK"
    var locale = "en-GB"
    var currency = "GBP"
    var origin = UI.PlaceModel(id: "EDI", name: "Edinburgh")
    var destination = UI.PlaceModel(id: "LHR", name: "London Heathrow")

    func fetchItineraries() {
        let date = Date().nextMonday
        let request = SessionRequest(
            country: .init(rawValue: country),
            locale: .init(rawValue: locale),
            currency: .init(rawValue: currency),
            originPlace: .init(rawValue: origin.id),
            destinationPlace: .init(rawValue: destination.id),
            outboundDate: date,
            inboundDate: date.nextDay,
            adults: 1
        )

        startSession(for: request)
    }

    init(presenter: ItineraryListPresenter, service: Service = makeService()) {
        self.presenter = presenter
        self.service = service
    }

    private unowned let presenter: ItineraryListPresenter
    private let service: Service

    private func startSession(for request: SessionRequest) {
        service.send(request).result { [weak self, origin, destination] result in
            guard let self = self else {
                return
            }

            switch result {
            case let .failure(error):
                self.presenter.show(
                    .failure(error),
                    request: request,
                    origin: origin,
                    destination: destination
                )
            case let .success(success):
                self.fetchPrices(
                    from: success.location,
                    request: request,
                    origin: origin,
                    destination: destination
                )
            }
        }
    }

    private func fetchPrices(
        from url: URL,
        request: SessionRequest,
        origin: UI.PlaceModel,
        destination: UI.PlaceModel) {
        service.send(PricingRequest(
            sessionUrl: url
        )).result { [weak self] result in
            guard let self = self else {
                return
            }
            if case let .success(response) = result, response.status == .pending {
                self.fetchPrices(from: url, request: request, origin: origin, destination: destination)
            }
            self.presenter.show(result, request: request, origin: origin, destination: destination)
        }
    }
}
