import UIKit
import DZNEmptyDataSet

final class ItineraryListViewControllerImpl: UIViewController, ItineraryListViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    override var prefersNavigationBarHidden: Bool {
        return true
    }

    func show(_ pricingModel: UI.PricingModel) {
        self.pricingModel = pricingModel
        reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactor.fetchItineraries()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigationBarState(animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ItineraryCell.self, forCellReuseIdentifier: "Itinerary")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        reloadData()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.contentInset = UIEdgeInsets(
            top: headerView.frame.maxY,
            left: 0,
            bottom: 0,
            right: 0
        )
    }

    // MARK: - UITableViewDataSource

    func tableView(_: UITableView, estimatedHeightForRowAt _: IndexPath) -> CGFloat {
        return 150
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let itenerary = pricingModel.iteneraries[indexPath.row]
        let layout = ItineraryCellLayout.make(width: tableView.bounds.width, itinerary: itenerary)
        return layout.outerSize.height
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return pricingModel.iteneraries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Itinerary", for: indexPath) as! ItineraryCell
        let itenerary = pricingModel.iteneraries[indexPath.row]

        cell.showItinerary(itenerary)

        return cell
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    // MARK: - DZNEmptyDataSetSource

    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string: String
        switch pricingModel.state {
        case .requested, .progress:
            string = "Flights requested".localized()
        case .completed:
            string = "No flights found".localized()
        case .failed:
            string = "Something went wrong".localized()
        }
        return NSAttributedString(string: string)
    }

    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let string: String
        switch pricingModel.state {
        case .requested, .progress:
            string = "Results should appear in next few seconds.".localized()
        case .completed:
            string = "Probably you can select other direction or time.".localized()
        case .failed:
            string = "We were not able to load flights list. Please try again.".localized()
        }
        return NSAttributedString(string: string)
    }

    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        switch pricingModel.state {
        case .failed:
            let string = "Reload".localized()
            return NSAttributedString(string: string)
        case .requested, .progress, .completed:
            return nil
        }
    }

    // MARK: - DZNEmptyDataSetDelegate

    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        interactor.fetchItineraries()
    }

    @IBOutlet
    private var tableView: UITableView!

    @IBOutlet
    private var headerLabel: UILabel!

    @IBOutlet
    private var detailLabel: UILabel!

    @IBOutlet
    private var resultsLabel: UILabel!

    @IBOutlet
    private var progressBar: UIProgressView!

    @IBOutlet
    private var headerView: UIView!

    private lazy var presenter = ItineraryListPresenterImpl(viewController: self)
    private lazy var interactor = ItineraryListInteractorImpl(presenter: presenter)

    private var pricingModel = UI.PricingModel(
        state: .requested,
        results: "",
        stations: .init(string: ""),
        time: .init(string: ""),
        iteneraries: []
    )

    private func reloadData() {
        guard isViewLoaded else {
            return
        }

        tableView.reloadData()
        headerLabel.attributedText = pricingModel.stations
        detailLabel.attributedText = pricingModel.time
        resultsLabel.text = pricingModel.results

        switch pricingModel.state {
        case .failed, .requested, .completed:
            progressBar.alpha = 0
        case let .progress(progress):
            progressBar.alpha = 1
            progressBar.progress = progress
        }
    }
}
