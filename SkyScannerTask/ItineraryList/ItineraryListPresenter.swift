import Foundation
import SkyApi

protocol ItineraryListPresenter: class {
    func show(
        _ result: Result<PricingResponse, Error>,
        request: SessionRequest,
        origin: UI.PlaceModel,
        destination: UI.PlaceModel
    )
}
