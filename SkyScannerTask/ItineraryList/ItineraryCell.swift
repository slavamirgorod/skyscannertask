import UIKit

final class ItineraryCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    func showItinerary(_ itinerary: UI.ItineraryModel) {
        self.itinerary = itinerary

        priceLabel.text = itinerary.price
        notesLabel.attributedText = itinerary.notes

        reuseLegViews(count: itinerary.legs.count)

        zip(legViews, itinerary.legs).forEach { legView, leg in
            legView.imageView.setImageSource(leg.carrierImage)
            legView.timeLabel.attributedText = leg.time
            legView.segmentsLabel.text = leg.segments
            legView.directnessLabel.text = leg.directness
            legView.durationLabel.text = leg.duration
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        if let itinerary = itinerary {
            let layout = ItineraryCellLayout.make(width: bounds.width, itinerary: itinerary)
            contentView.frame = layout.contentFrame
            priceLabel.frame = layout.priceFrame
            notesLabel.frame = layout.notesFrame

            zip(legViews, layout.legs).forEach { legView, legLayout in
                legView.frame = legLayout.outerFrame
                legView.imageView.frame = legLayout.imageFrame
                legView.timeLabel.frame = legLayout.timeFrame
                legView.segmentsLabel.frame = legLayout.segmentsFrame
                legView.directnessLabel.frame = legLayout.directnessFrame
                legView.durationLabel.frame = legLayout.durationFrame
            }
        }
    }

    private class LegView: UIView {
        let imageView = UrlImageView(frame: .zero)
        let timeLabel = UILabel(frame: .zero)
        let segmentsLabel = UILabel(frame: .zero)
        let directnessLabel = UILabel(frame: .zero)
        let durationLabel = UILabel(frame: .zero)

        override init(frame: CGRect) {
            super.init(frame: frame)
            setUp()
        }

        required init?(coder: NSCoder) {
            super.init(coder: coder)
            setUp()
        }

        private func setUp() {
            timeLabel.font = .systemFont(ofSize: 16, weight: .regular)
            segmentsLabel.font = .systemFont(ofSize: 12, weight: .regular)
            segmentsLabel.textColor = UI.BrandColor.gray
            imageView.contentMode = .scaleAspectFit
            directnessLabel.font = .systemFont(ofSize: 16, weight: .regular)
            directnessLabel.textAlignment = .right
            durationLabel.textAlignment = .right
            durationLabel.font = .systemFont(ofSize: 12, weight: .regular)
            durationLabel.textColor = UI.BrandColor.gray

            addSubview(imageView)
            addSubview(timeLabel)
            addSubview(segmentsLabel)
            addSubview(directnessLabel)
            addSubview(durationLabel)
            addSubview(timeLabel)
        }
    }

    private let priceLabel = UILabel(frame: .zero)
    private let notesLabel = UILabel(frame: .zero)
    private var legViews = [LegView]()
    private var itinerary: UI.ItineraryModel?

    private func reuseLegViews(count: Int) {
        let currentCount = legViews.count
        if currentCount < count {
            (currentCount ..< count).forEach { _ in
                let legView = LegView(frame: .zero)
                legViews.append(legView)
                addSubview(legView)
            }
        } else if count < currentCount {
            (count ..< currentCount).forEach { _ in
                let legView = legViews.removeLast()
                legView.removeFromSuperview()
            }
        }
    }

    private func setUp() {
        priceLabel.textAlignment = .right
        priceLabel.font = .systemFont(ofSize: 20, weight: .regular)
        notesLabel.font = .systemFont(ofSize: 12, weight: .regular)
        notesLabel.textColor = UI.BrandColor.gray

        contentView.addSubview(priceLabel)
        contentView.addSubview(notesLabel)

        backgroundColor = .clear
        contentView.backgroundColor = .white
    }
}
