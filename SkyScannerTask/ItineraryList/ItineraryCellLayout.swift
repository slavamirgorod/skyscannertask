//
//  ItineraryCellLayout.swift
//  SkyScannerTask
//
//  
//
//

import CoreGraphics
import Foundation

struct ItineraryCellLayout {
    struct LegLayout {
        let imageFrame: CGRect
        let timeFrame: CGRect
        let segmentsFrame: CGRect
        let directnessFrame: CGRect
        let durationFrame: CGRect
        let outerFrame: CGRect

        static func make(frame: CGRect) -> LegLayout {
            let width = frame.width
            let height = frame.height
            let midY = height / 2

            return .init(
                imageFrame: CGRect(x: 0, centerY: midY, side: imageSize),
                timeFrame: CGRect(x: imageSize + elemMargin, bottomY: midY, width: width / 2, height: timeHeight),
                segmentsFrame: CGRect(x: imageSize + elemMargin, y: midY, width: width / 2, height: segmentsHeight),
                directnessFrame: CGRect(rightX: width, bottomY: midY, width: width / 2, height: directnessHeight),
                durationFrame: CGRect(rightX: width, y: midY, width: width / 2, height: durationHeight),
                outerFrame: frame
            )
        }

        private static let imageSize = CGFloat(31)
        private static let elemMargin = CGFloat(10)
        private static let timeHeight = CGFloat(18)
        private static let segmentsHeight = CGFloat(16)
        private static let directnessHeight = CGFloat(16)
        private static let durationHeight = CGFloat(16)
    }

    let legs: [LegLayout]
    let priceFrame: CGRect
    let notesFrame: CGRect
    let contentFrame: CGRect
    let outerSize: CGSize

    static func make(width: CGFloat, itinerary: UI.ItineraryModel) -> ItineraryCellLayout {
        let minX = outerMargins.width
        let maxX = width - outerMargins.width
        let effectiveWidth = maxX - minX
        var currentY = outerMargins.height

        let legs: [LegLayout] = itinerary.legs.indices.map { index in
            let y = currentY + CGFloat(index) * legHeight
            let frame = CGRect(x: minX, y: y, width: effectiveWidth, height: legHeight)
            return .make(frame: frame)
        }

        currentY += CGFloat(itinerary.legs.count) * legHeight + legsMargin

        let priceFrame = CGRect(rightX: maxX, y: currentY, width: effectiveWidth / 2, height: priceHeight)
        let notesFrame = CGRect(x: minX, y: currentY, width: effectiveWidth, height: notesHeight)
        let maxY = max(priceFrame.maxY, notesFrame.maxY)
        let outerSize = CGSize(width: width, height: maxY + outerMargins.height)
        return ItineraryCellLayout(
            legs: legs,
            priceFrame: priceFrame,
            notesFrame: notesFrame,
            contentFrame: CGRect(
                x: 0,
                y: verticalInsets,
                width: outerSize.width,
                height: outerSize.height
            ),
            outerSize: CGSize(width: outerSize.width, height: outerSize.height + 2 * verticalInsets)
        )
    }

    private static let legHeight = CGFloat(48)
    private static let legsMargin = CGFloat(10)
    private static let outerMargins = CGSize(width: 16, height: 12)
    private static let priceHeight = CGFloat(20)
    private static let notesHeight = CGFloat(16)
    private static let verticalInsets = CGFloat(6)
}
