import Foundation

protocol ItineraryListViewController: class {
    func show(_ pricingModel: UI.PricingModel)
}
