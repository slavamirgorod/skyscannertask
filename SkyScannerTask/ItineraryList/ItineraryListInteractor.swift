import Foundation

protocol ItineraryListInteractor {
    var origin: UI.PlaceModel { get set }
    var destination: UI.PlaceModel { get set }

    func fetchItineraries()
}
