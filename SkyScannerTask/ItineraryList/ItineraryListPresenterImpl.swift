import Foundation
import SkyApi
import SkyCore

final class ItineraryListPresenterImpl: ItineraryListPresenter {
    func show(
        _ result: Result<PricingResponse, Error>,
        request: SessionRequest,
        origin: UI.PlaceModel,
        destination: UI.PlaceModel) {
        switch result {
        case let .failure(error):
            show(error, request: request, origin: origin, destination: destination)
        case let .success(success):
            show(success, request: request, origin: origin, destination: destination)
        }
    }

    init(viewController: ItineraryListViewController,
         mainQueue: @escaping ActionDispatcher = dispatchOnMainQueue,
         backgroundQueue: @escaping ActionDispatcher = dispatchOnUserInitiatedQueue) {
        self.viewController = viewController
        self.mainQueue = mainQueue
        self.backgroundQueue = backgroundQueue
    }

    private unowned let viewController: ItineraryListViewController
    private let mainQueue: ActionDispatcher
    private let backgroundQueue: ActionDispatcher

    private func show(
        _ response: PricingResponse,
        request: SessionRequest,
        origin: UI.PlaceModel,
        destination: UI.PlaceModel) {
        backgroundQueue { [weak self] in
            let pricingModel = response.pricingModel(
                request: request,
                origin: origin,
                destination: destination
            )

            if let self = self {
                self.mainQueue { self.viewController.show(pricingModel) }
            }
        }
    }

    private func show(
        _ error: Error,
        request: SessionRequest,
        origin: UI.PlaceModel,
        destination: UI.PlaceModel) {
        let pricingModel = UI.PricingModel(
            state: .failed,
            results: "Updates failed".localized(),
            stations: destination.segmentString(from: origin),
            time: request.time,
            iteneraries: []
        )
        viewController.show(pricingModel)
    }
}

private enum Env {
    static let secondsInMinute = 60

    static var timeFormatter: DateFormatter = {
        let timeFormatter = DateFormatter()
        timeFormatter.dateStyle = .none
        timeFormatter.timeStyle = .short
        return timeFormatter
    }()

    static var monthDayFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM, d, EEE"
        return dateFormatter
    }()

    static var durationFormatter: DateComponentsFormatter = {
        let componentsFormatter = DateComponentsFormatter()
        componentsFormatter.allowedUnits = [.day, .hour, .minute]
        componentsFormatter.unitsStyle = .abbreviated
        return componentsFormatter
    }()
}

private extension Leg {
    func legModel(places: [StationID: Station], carriers: [CarrierID: Carrier]) -> UI.LegModel {
        let duration = Env.durationFormatter.string(from: TimeInterval(self.duration * Env.secondsInMinute))
        let stationCodes = ([places[originStation]] + [places[destinationStation]]).compactMap { $0?.code }
        let directness = String.localizedSegmentsCount(segmentIds.count)
        let carrierImages = self.carriers.compactMap { carriers[$0]?.imageUrl }
        let carrierCodes = self.carriers.compactMap { carriers[$0]?.displayCode }
        let segments = stationCodes.joined(separator: "-") + ", " + carrierCodes.joined(separator: " + ")
        let carrierImage: ImageSource = carrierImages.count == 1 ? .url(carrierImages[0]) : .image(UI.BrandImage.multiCarrier)
        let time = timeString(scriptOffset: 4, scriptColor: UI.BrandColor.red)

        return UI.LegModel(
            time: time,
            duration: duration ?? "",
            segments: segments,
            directness: directness,
            carrierImage: carrierImage
        )
    }

    private func timeString(scriptOffset: CGFloat, scriptColor: UIColor) -> NSAttributedString {
        let departure = Env.timeFormatter.string(from: self.departure)
        let arrival = Env.timeFormatter.string(from: self.arrival)
        let days = self.arrival.days(from: self.departure)
        let string = NSMutableAttributedString(string: "\(departure) - \(arrival)")
        if days != 0 {
            let prefix = days > 0 ? " +" : " "
            string.append(
                prefix + "\(days)",
                attributes: [.baselineOffset: scriptOffset, .foregroundColor: scriptColor]
            )
        }
        return string
    }
}

private extension SessionRequest {
    var time: NSAttributedString {
        let string = [
            Env.monthDayFormatter.string(from: outboundDate),
            inboundDate.map(Env.monthDayFormatter.string)
        ].compactMap { $0 }.joined(separator: " - ")
        return NSAttributedString(string: string)
    }
}

private extension UI.PlaceModel {
    func segmentString(from origin: UI.PlaceModel) -> NSAttributedString {
        return NSAttributedString(string: "\(origin.name) - \(name)")
    }
}

private extension PricingResponse {
    func pricingModel(
        request: SessionRequest,
        origin: UI.PlaceModel,
        destination: UI.PlaceModel) -> UI.PricingModel {
        let currencyFomatter = NumberFormatter()
        currencyFomatter.numberStyle = .currency
        currencyFomatter.locale = Locale(identifier: query.locale.rawValue)
        currencyFomatter.maximumFractionDigits = 0

        let keyedLegs = legs.makeKeyed()
        let keyedCarriers = carriers.makeKeyed()
        let keyedPlaces = places.makeKeyed()
        let cheapestPrices = itineraries.cheapestPrices(limit: 3)
        let lowestDurations = itineraries.lowestDurations(using: keyedLegs, limit: 3)

        let iteneraries: [UI.ItineraryModel] = itineraries.map { itenerary in
            let pricingOption = itenerary.cheapestPricingOption
            let price = currencyFomatter.string(from: NSNumber(value: pricingOption.price))

            let allureNotes = [
                cheapestPrices
                    .firstIndex(where: { itenerary.cheapestPrice <= $0 })
                    .map { String.localizedCheapPosition($0) },
                lowestDurations
                    .firstIndex(where: { itenerary.legsDuration(using: keyedLegs) <= $0 })
                    .map { String.localizedFastPosition($0) },
            ].compactMap { $0 }.joined(separator: " ")

            let bookingNotes = String.localizedBookingsCount(pricingOption.agents.count)

            let notes: NSAttributedString = {
                let string = NSMutableAttributedString()
                string.append(allureNotes, attributes: [.foregroundColor: UI.BrandColor.green])
                string.append(bookingNotes, attributes: [.foregroundColor: UI.BrandColor.gray])
                return string
            }()

            let legModels: [UI.LegModel] = itenerary.legs(using: keyedLegs).map { leg in
                leg.legModel(places: keyedPlaces, carriers: keyedCarriers)
            }

            return UI.ItineraryModel(
                price: price ?? "",
                notes: notes,
                legs: legModels
            )
        }

        let results: String
        switch status {
        case .complete:
            results = .localizedResultsCount(itineraries.count)
        case .pending:
            results = "In progress".localized()
        }

        return UI.PricingModel(
            state: self.state(),
            results: results,
            stations: destination.segmentString(from: origin),
            time: request.time,
            iteneraries: iteneraries
        )
    }

    private func state() -> UI.PricingState {
        switch status {
        case .complete:
            return .completed
        case .pending:
            let finishedCount = agents.reduce(0) { $0 + $1.statusUnion }
            return .progress(Float(finishedCount) / Float(agents.count))
        }
    }
}

private extension Agent {
    var statusUnion: Int {
        switch status {
        case .complete: return 1
        case .pending: return 0
        }
    }
}

private extension String {
    static func localizedResultsCount(_ count: Int) -> String {
        return localizedStringWithFormat("%d results", count)
    }

    static func localizedCheapPosition(_ position: Int) -> String {
        return position != 0 ? localizedStringWithFormat("%d in cheap", position + 1) : "Cheapests".localized()
    }

    static func localizedFastPosition(_ position: Int) -> String {
        return position != 0 ? localizedStringWithFormat("%d in fast", position + 1) : "Fastest".localized()
    }

    static func localizedSegmentsCount(_ count: Int) -> String {
        return count < 2 ? "Direct".localized() : .localizedStringWithFormat("%d transfers", count)
    }

    static func localizedBookingsCount(_ count: Int) -> String {
        return count < 2 ? "" : .localizedStringWithFormat("%d bookings required", count)
    }
}
