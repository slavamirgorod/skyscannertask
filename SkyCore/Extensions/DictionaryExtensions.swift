import Foundation

public extension Dictionary {
    init<S: Sequence>(_ seq: S) where S.Element == (Key, Value) {
        self.init()
        for (key, value) in seq { self[key] = value }
    }

    func filteringNilValues<T>() -> [Key: T] where Value == T? {
        return [Key: T](compactMap {
            switch $0 {
            case (_, .none):
                return nil
            case let (key, .some(value)):
                return (key, value)
            }
        })
    }
}
