import Foundation

public extension Optional {
    enum Error: Swift.Error {
        case valueIsNil
    }

    func get(elseThrow error: Swift.Error = Error.valueIsNil) throws -> Wrapped {
        guard let value = self else { throw error }
        return value
    }
}
