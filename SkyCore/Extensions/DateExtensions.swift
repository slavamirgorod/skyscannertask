import Foundation

public extension Date {
    var yearMonthDayString: String {
        return Date.yearMonthDayFormatter.string(from: self)
    }

    var nextMonday: Date {
        let calendar = Calendar(identifier: .gregorian)
        let weekday = calendar.component(.weekday, from: self)
        let monday = 2
        let movedays = weekday - monday
        let components = DateComponents(weekday: movedays <= 0 ? movedays + 7 : movedays)
        return calendar.date(byAdding: components, to: self)!
    }

    var nextDay: Date {
        return addingTimeInterval(24 * 60 * 60)
    }

    func days(from date: Date) -> Int {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.day], from: date, to: self)
        return components.day ?? 0
    }

    private static let yearMonthDayFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
}
