//
//  SkyCore.h
//  SkyCore
//
//  Created by Viacheslav Mirgorod on 5/12/19.
//  Copyright © 2019 Anonimous. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SkyCore.
FOUNDATION_EXPORT double SkyCoreVersionNumber;

//! Project version string for SkyCore.
FOUNDATION_EXPORT const unsigned char SkyCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SkyCore/PublicHeader.h>


