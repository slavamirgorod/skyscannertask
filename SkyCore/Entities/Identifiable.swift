import Foundation

public protocol Identifiable {
    associatedtype Identifier: Hashable

    var id: Identifier { get }
}

public extension Array where Element: Identifiable {
    func makeKeyed() -> [Element.Identifier: Element] {
        return Dictionary(map { ($0.id, $0) }, uniquingKeysWith: { $1 })
    }
}
