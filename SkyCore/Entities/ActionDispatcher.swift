import Foundation

public typealias ActionDispatcher = (@escaping Action) -> Void

public func dispatchOnMainQueue(_ block: @escaping () -> Void) {
    DispatchQueue.main.async(execute: block)
}

public func dispatchOnUserInitiatedQueue(_ block: @escaping () -> Void) {
    DispatchQueue.global(qos: .userInitiated).async(execute: block)
}
