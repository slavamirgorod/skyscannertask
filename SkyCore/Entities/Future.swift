import Foundation

public final class Future<T> {
    public typealias Result = Swift.Result<T, Swift.Error>

    public enum Error: Swift.Error {
        case valueIsNil
    }

    public init(payload: T) {
        state = .fulfilled(.success(payload))
    }

    public init(error: Error) {
        state = .fulfilled(.failure(error))
    }

    public static func create(of type: T.Type = T.self) -> (Future<T>, feed: (Result) -> Void) {
        let future = Future<T>()
        return (future, future.accept)
    }

    public func succeded(_ callback: @escaping (T) -> Void) {
        state.process(callback: { $0.accept(callback) })
    }

    public func failed(_ callback: @escaping (Swift.Error) -> Void) {
        state.process(callback: { $0.accept(callback) })
    }

    public func result(_ callback: @escaping (Result) -> Void) {
        state.process(callback: callback)
    }

    public func get() throws -> T {
        switch state {
        case .pending(_):
            throw Error.valueIsNil
        case let .fulfilled(result):
            switch result {
            case let .success(payload):
                return payload
            case let .failure(error):
                throw error
            }
        }
    }

    private var state: State

    private init() {
        state = .initial()
    }

    private func accept(result: Result) {
        state.fulfill(with: result)
    }

    private indirect enum State {
        case pending([(Result) -> Void])
        case fulfilled(Result)

        mutating func process(callback: @escaping (Result) -> Void) {
            switch self {
            case .pending(let callbacks):
                self = .pending(callbacks + [callback])
            case .fulfilled(let result):
                callback(result)
            }
        }

        mutating func fulfill(with result: Result) {
            guard case .pending(let callbacks) = self else { return }
            self = .fulfilled(result)
            callbacks.forEach { $0(result) }
        }

        static func initial() -> State {
            return .pending([])
        }
    }
}

public extension Future {
    func then<U>(_ transform: @escaping (T) throws -> U) -> Future<U> {
        let (future, feed) = Future<U>.create()
        succeded { payload in
            do {
                try feed(.success(transform(payload)))
            }
            catch let error {
                feed(.failure(error))
            }
        }
        failed { error in
            feed(.failure(error))
        }
        return future
    }

    func then<U>(_ transform: @escaping (T) -> Future<U>) -> Future<U> {
        let (future, feed) = Future<U>.create()
        succeded { payload in
            let next = transform(payload)
            next.succeded { feed(.success($0)) }
            next.failed { feed(.failure($0)) }
        }
        failed { error in feed(.failure(error)) }
        return future
    }
}

private extension Result {
    func accept(_ visitor: (Success) -> Void) {
        guard case .success(let payload) = self else { return }
        visitor(payload)
    }

    func accept(_ visitor: (Failure) -> Void) {
        guard case .failure(let error) = self else { return }
        visitor(error)
    }
}
