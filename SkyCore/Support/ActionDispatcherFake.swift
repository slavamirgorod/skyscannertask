import Foundation

public final class ActionDispatcherFake {
    public private(set) var calls = [Action]()

    public func dispatch(_ block: @escaping Action) {
        calls.append(block)
    }

    public func finish() {
        calls.forEach { $0() }
    }

    public init() {
        
    }
}
