@testable import SkyScannerTask
import XCTest
import SkyApi
import SkyCore

class ItineraryListInteractorImplTests: XCTestCase {
    private let presenter = ItineraryListPresenterFake()
    private let service = ServiceFake()
    private lazy var me = ItineraryListInteractorImpl(presenter: presenter, service: service)

    func test_WhenAskedToFetchItineraries_StartsSessionRequest() {
        me.fetchItineraries()

        let request = service.postCalls.last?.encodable as? SessionRequest
        XCTAssertEqual(request?.locale, LocaleID(rawValue: me.locale))
        XCTAssertEqual(request?.country, CountryID(rawValue: me.country))
        XCTAssertEqual(request?.currency, CurrencyID(rawValue: me.currency))
        XCTAssertEqual(request?.originPlace, PlaceID(rawValue: me.origin.id))
        XCTAssertEqual(request?.destinationPlace, PlaceID(rawValue: me.destination.id))
    }

    func test_WhenSessionStarts_FetchesPrices() {
        me.fetchItineraries()
        let feed = service.postCalls.last?.feed as? (Future<SessionResponse>.Result) -> Void
        feed?(.success(.init(location: sampleUrl)))

        let request = service.getCalls.last?.encodable as? PricingRequest
        XCTAssertEqual(request?.sessionUrl, sampleUrl)
    }

    func test_WhenPricesFetched_AndStatusNotComplete_AsksAgain() {
        me.fetchItineraries()
        let sessionFeed = service.postCalls.last?.feed as? (Future<SessionResponse>.Result) -> Void
        sessionFeed?(.success(.init(location: sampleUrl)))
        let priceFeed = service.getCalls.last?.feed as? (Future<PricingResponse>.Result) -> Void
        priceFeed?(.success(pendingPricingResponse))

        let requests = service.getCalls.compactMap { $0.encodable as? PricingRequest }
        XCTAssertEqual(requests.count, 2)
    }

    func test_WhenPricesFetched_AsksPresenterToShow() throws {
        me.fetchItineraries()
        let sessionFeed = service.postCalls.last?.feed as? (Future<SessionResponse>.Result) -> Void
        sessionFeed?(.success(.init(location: sampleUrl)))
        let priceFeed = service.getCalls.last?.feed as? (Future<PricingResponse>.Result) -> Void
        priceFeed?(.success(completePricingResponse))

        let call = presenter.showCalls.last
        XCTAssertEqual(try call?.result.get(), completePricingResponse)
    }
}

private let sampleUrl = URL(string: "http://skyscanner.com")!
private let pendingPricingResponse = PricingResponse(
    query: .empty,
    status: .pending,
    legs: [],
    carriers: [],
    places: [],
    itineraries: [],
    currencies: [],
    agents: []
)
private let completePricingResponse = PricingResponse(
    query: .empty,
    status: .complete,
    legs: [],
    carriers: [],
    places: [],
    itineraries: [],
    currencies: [],
    agents: []
)
