@testable import SkyScannerTask
import XCTest
import SkyApi
import SkyCore

class ItineraryListPresenterImplTests: XCTestCase {
    private let mainQueue = ActionDispatcherFake()
    private let backgroundQueue = ActionDispatcherFake()
    private let viewController = ItineraryListViewControllerFake()
    private lazy var me = ItineraryListPresenterImpl(
        viewController: viewController,
        mainQueue: mainQueue.dispatch,
        backgroundQueue: backgroundQueue.dispatch
    )

    func test_WhenAskedToShowItineraries_StartsBackgroundTaskForProcessing() {
        me.show(.success(pricingResponse), request: sessionRequest, origin: origin, destination: destination)

        XCTAssertEqual(backgroundQueue.calls.count, 1)
    }

    func test_WhenAskedToShowItineraries_AsksViewControllerToShowPreparedResults() {
        me.show(.success(pricingResponse), request: sessionRequest, origin: origin, destination: destination)
        backgroundQueue.finish()
        mainQueue.finish()
    
        XCTAssertEqual(mainQueue.calls.count, 1)
        XCTAssertFalse(viewController.showCalls.isEmpty)
    }
}

private let pricingResponse = PricingResponse(
    query: .empty,
    status: .complete,
    legs: [],
    carriers: [],
    places: [],
    itineraries: [],
    currencies: [],
    agents: []
)

private let sessionRequest = SessionRequest(
    country: "UK",
    locale: "en-GB",
    currency: "GBP",
    originPlace: "EDI",
    destinationPlace: "LHR",
    outboundDate: Date(),
    inboundDate: Date(),
    adults: 1
)

private let origin = UI.PlaceModel(id: "EDI", name: "Edinburgh")
private let destination = UI.PlaceModel(id: "LHR", name: "London Heathrow")
