@testable import SkyScannerTask
import SkyApi
import Foundation

final class ItineraryListPresenterFake: ItineraryListPresenter {
    struct Show {
        let result: Result<PricingResponse, Error>
        let request: SessionRequest
        let origin: UI.PlaceModel
        let destination: UI.PlaceModel
    }

    private(set) var showCalls = [Show]()

    func show(
        _ result: Result<PricingResponse, Error>,
        request: SessionRequest,
        origin: UI.PlaceModel,
        destination: UI.PlaceModel) {
        showCalls.append(.init(result: result, request: request, origin: origin, destination: destination))
    }
}
