@testable import SkyScannerTask
import SkyApi
import Foundation

final class ItineraryListViewControllerFake: ItineraryListViewController {
    func show(_ pricingModel: UI.PricingModel) {
        showCalls.append(.init(pricingModel: pricingModel))
    }

    struct Show {
        let pricingModel: UI.PricingModel
    }

    private(set) var showCalls = [Show]()
}
