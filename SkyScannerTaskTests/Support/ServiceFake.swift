import Foundation
import SkyApi
import SkyCore

final class ServiceFake: Service {
    struct GetCall {
        let encodable: GetEncodable
        let feed: Any
    }

    struct PostCall {
        let encodable: PostEncodable
        let feed: Any
    }

    private(set) var getCalls = [GetCall]()
    private(set) var postCalls = [PostCall]()

    func send<T>(_ encodable: GetEncodable, of type: T.Type) -> Future<T> where T : Decodable {
        let (future, feed) = Future<T>.create(of: type)
        getCalls.append(.init(encodable: encodable, feed: feed))
        return future
    }

    func send<T>(_ encodable: PostEncodable, of type: T.Type) -> Future<T> where T : Decodable {
        let (future, feed) = Future<T>.create(of: type)
        postCalls.append(.init(encodable: encodable, feed: feed))
        return future
    }
}
