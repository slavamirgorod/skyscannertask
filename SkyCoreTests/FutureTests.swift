@testable import SkyCore
import XCTest

final class FutureTests: XCTestCase {
    func test_WhenNotFulfilled_ThrowsErrorOnGet() {
        let (future, _) = Future.create(of: Int.self)

        XCTAssertThrowsError(try future.get())
    }

    func test_WhenFulfilledWithFailure_ThrowsErrorOnGet() {
        let (future, feed) = Future.create(of: Int.self)

        feed(.failure(error))

        XCTAssertThrowsError(try future.get())
    }

    func test_WhenFulfilledWithSuccess_ReturnsValue() throws {
        let (future, feed) = Future.create(of: Int.self)

        feed(.success(success))

        XCTAssertEqual(success, try future.get())
    }

    func test_WhenFulfilledWithSuccess_NotifiesObservers() throws {
        let (future, feed) = Future.create(of: Int.self)
        var result: Int?
        future.succeded { result = $0 }

        feed(.success(success))

        XCTAssertEqual(success, result)
    }

    func test_WhenFulfilledWithFailure_NotifiesObservers() throws {
        let (future, feed) = Future.create(of: Int.self)
        var result: Error?
        future.failed { result = $0 }

        feed(.failure(error))

        XCTAssertNotNil(result)
    }
}

let success = 42
let error = Future<Any>.Error.valueIsNil
