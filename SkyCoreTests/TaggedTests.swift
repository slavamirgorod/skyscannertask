@testable import SkyCore
import XCTest

final class TaggedTests: XCTestCase {
    func test_TagEncodesToSingleValue() {
        XCTAssertNoThrow({
            let data = try JSONEncoder().encode([User(id: "187")])
            let json = try String(data: data, encoding: .utf8).get()
            XCTAssertEqual(json, User.expectedJSON)
        })
    }

    func test_TagDecodesFromSingleValue() {
        XCTAssertNoThrow({
            let data = try User.expectedJSON.data(using: .utf8).get()
            let users = try JSONDecoder().decode([User].self, from: data)
            XCTAssertEqual(users.count, 1)
            XCTAssertEqual(users.first?.id, "187")
        })
    }

    func test_TagEncodesAndDecodesToSingleValueWithOptionalRawTypeAndNilValue() {
        struct Container: Codable {
            typealias Idenitifer = Tagged<Container, String?>
            let id: Idenitifer
        }

        XCTAssertNoThrow({
            let data = try JSONEncoder().encode([Container(id: nil)])
            let containers = try JSONDecoder().decode([Container].self, from: data)
            XCTAssertEqual(containers.count, 1)
            XCTAssertEqual(containers.first?.id.rawValue, nil)
        })
    }

    func test_TagDescriptionIsTransparent() {
        let value = [1, 2, 3]
        let tagged = Tagged<TaggedTests, [Int]>(rawValue: value)
        XCTAssertEqual(String(describing: value), String(describing: tagged))
    }

    func test_TagDebugDescriptionIsTransparent() {
        let value = [1, 2, 3]
        let tagged = Tagged<TaggedTests, [Int]>(rawValue: value)
        XCTAssertEqual(String(reflecting: value), String(reflecting: tagged))
    }
}

private struct User: Codable {
    typealias Idenitifer = Tagged<User, String>
    let id: Idenitifer

    static let expectedJSON = "[{\"id\":\"187\"}]"
}
